package it.fastcode.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MimeType implements Comparable<String> {

	private @Id @GeneratedValue Long id;
	private String name;
	private String mimeType;

	public MimeType() {
	}

	public MimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public MimeType(String name, String mimeType) {

		this.name = name;
		this.mimeType = mimeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((mimeType == null) ? 0 : mimeType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MimeType other = (MimeType) obj;
		if (mimeType == null) {
			if (other.mimeType != null)
				return false;
		} else if (!mimeType.equals(other.mimeType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MimeType [id=" + id + ", name=" + name + ", mimeType=" + mimeType + "]";
	}

	@Override
	public int compareTo(String o) {
		// TODO Auto-generated method stub
		return this.getMimeType().equals(o) ? 1 : -1;
	}

}