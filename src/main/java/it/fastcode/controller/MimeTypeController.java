package it.fastcode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.fastcode.business.FolderMimeType;

@RestController
public class MimeTypeController {

	@Autowired
	private FolderMimeType service;


	@RequestMapping(value = "/checkfolder", method = RequestMethod.GET, produces = "application/json")
	String checkFolder(@RequestParam(name = "folder", required = true) String folder) {

		try {
			return service.checkFolder(folder);
		} catch (Exception e) {
			return e.getMessage();
		}
	}

}