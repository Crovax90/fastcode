package it.fastcode;

import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastcodeApplication {

	public static void main(String[] args) throws CertificateException, NoSuchProviderException, FileNotFoundException {
		SpringApplication.run(FastcodeApplication.class, args);
	}

}
