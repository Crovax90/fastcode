package it.fastcode.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import it.fastcode.model.MimeType;

public interface MimeTypeRepository extends JpaRepository<MimeType, Long> {

}