package it.fastcode.business;

import java.io.File;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.fastcode.exception.ErrorInDataException;
import it.fastcode.model.EsitoRichiesta;
import it.fastcode.model.MimeType;
import it.fastcode.repository.MimeTypeRepository;

@Component
public class FolderMimeType {

	@Autowired
	private MimeTypeRepository repository;

	public String checkFolder(String path) throws Exception {
		String jsonString = null;
		FileSystem fs = FileSystems.getDefault();
		Path dir = fs.getPath(path);
		File fileDir = dir.toFile();

		if (fileDir.exists() && fileDir.isDirectory()) {

			List<MimeType> listMime = repository.findAll();

			List<EsitoRichiesta> res = processAllFileInDirectory(fileDir, listMime, 0);

			ObjectMapper mapper = new ObjectMapper();
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(res);

		} else {
			throw new ErrorInDataException("La directory in Input non esiste [%s]", fileDir);
		}
		return jsonString;
	}

	private List<EsitoRichiesta> processAllFileInDirectory(File fileDir, List<MimeType> listMime, int level)
			throws Exception {
		List<EsitoRichiesta> res = new ArrayList<EsitoRichiesta>();

		if (level > 3)
			return res;

		for (File file : fileDir.listFiles()) {

			if (file.isHidden() || !file.canRead())
				continue;

			if (file.isDirectory()) {
				res.addAll(processAllFileInDirectory(file, listMime, ++level));
			} else {

				String mimeType = Files.probeContentType(file.toPath());
				
				if (listMime.contains(new MimeType(mimeType))) {
					res.add(new EsitoRichiesta(file.getName(), true));
				} else {
					res.add(new EsitoRichiesta(file.getName(), false));
				}
			}
		}

		return res;
	}

}
