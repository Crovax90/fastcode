package it.fastcode.configuration;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import it.fastcode.model.MimeType;
import it.fastcode.repository.MimeTypeRepository;

@Configuration
class LoadDatabase {
	
	@Bean
	CommandLineRunner initDatabase(MimeTypeRepository repository) {

		return args -> {
			repository.save(new MimeType("pdf", "application/pdf"));
			repository.save(new MimeType("txt", "text/plain"));
		};
	}
}