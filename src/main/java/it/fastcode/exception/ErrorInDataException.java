package it.fastcode.exception;

public class ErrorInDataException extends Exception {

	public ErrorInDataException(String message, Object... args) {
		super(String.format(message, args));
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
